const every = require('lodash/fp/every');
const rest = require('lodash/fp/rest');
const identity = require('lodash/fp/identity');
const {parseDateSegment} = require('./utils');

const isBetween = (beginning, end) => value => value >= beginning && value <= end;

const isValidYearRange = isBetween(1901, 2999);
const isValidMonthRange = isBetween(1, 12);
const isValidDayRange = isBetween(1, 31);

const allConditionsArePassed = rest(every(identity));

module.exports = dateString => {
  const [year, month, day] = dateString.split('-').map(parseDateSegment);
  return allConditionsArePassed(isValidYearRange(year), isValidMonthRange(month), isValidDayRange(day));
};
