const range = require('lodash/fp/range');
const sum = require('lodash/fp/sum');
const {isLeapYear, parseDateSegment, compareDates} = require('./utils');

const DAYS_IN_YEAR = 365;
const DAYS_IN_MONTH = 31;
const DAY_DIFF_PER_MONTH = [0, 3, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0];
const DAY_DIFF_IN_LEAP_YEAR_PER_MONTH = [0, 2].concat(DAY_DIFF_PER_MONTH.slice(2));

const getDayAdjustmentsPerMonthInGivenYear = inLeapYear => inLeapYear ? DAY_DIFF_IN_LEAP_YEAR_PER_MONTH : DAY_DIFF_PER_MONTH;

const excludePartialDays = dayDiff => dayDiff -1;

const createMonthsRange = (startMonth, endMonth) =>
  startMonth > endMonth ? range(endMonth, startMonth) : range(startMonth, endMonth);

const calculateDayAdjustmentsForPassedMonths = (endDateYear, startMonth, endMonth) => {
  if (startMonth === endMonth) {
    return 0;
  } else {
    const daysInEndDateYearMonths = getDayAdjustmentsPerMonthInGivenYear(isLeapYear(endDateYear));
    const isStartMonthAfterEndMonth = startMonth > endMonth;
    const subtractOrAddCalculatedDays = isStartMonthAfterEndMonth ? 1 : -1;
    const monthsRange = createMonthsRange(startMonth, endMonth);

    return subtractOrAddCalculatedDays * monthsRange
      .reduce((daysTotal, monthIndex) => daysTotal + daysInEndDateYearMonths[monthIndex - 1], 0);
  }
};

const howManyLeapYearsBetweenYears = (yearFrom, yearTo) => range(yearFrom, yearTo).filter(isLeapYear).length;

const calculateDaysDiff = (dateA, dateB) => {
  const [startDate, endDate] = [dateA, dateB].sort(compareDates);
  const [startYear, startMonth, startDay] = startDate.split('-').map(parseDateSegment);
  const [endYear, endMonth, endDay] = endDate.split('-').map(parseDateSegment);

  const yearDiff = endYear - startYear;
  const monthDiff = endMonth - startMonth;
  const dayDiff = endDay - startDay;

  const monthLengthsAdjustment = calculateDayAdjustmentsForPassedMonths(endYear, startMonth, endMonth);

  const yearDiffInDays = yearDiff * DAYS_IN_YEAR;
  const monthDiffInDays = monthDiff * DAYS_IN_MONTH;

  const totalDayDiff = sum([
    yearDiffInDays,
    monthDiffInDays,
    dayDiff,
    howManyLeapYearsBetweenYears(startYear, endYear),
    monthLengthsAdjustment
  ]);

  return excludePartialDays(totalDayDiff);
};

module.exports = calculateDaysDiff;
