const parseInt = require('lodash/fp/parseInt');

const isNotZero = value => value !== 0;

const parseDateSegment = parseInt(10);

const isLeapYear = year => year % 4 === 0 && !(year % 100 === 0 && year % 400 > 0);

const compareDates = (dateA, dateB) => {
  const parsedDateA = dateA.split('-').map(parseDateSegment);
  const parsedDateB = dateB.split('-').map(parseDateSegment);

  const datePartDiffs = parsedDateA.map((datePart, datePartIndex) => datePart - parsedDateB[datePartIndex]);
  return datePartDiffs.find(isNotZero) || 0;
};

module.exports = {
  isLeapYear,
  parseDateSegment,
  compareDates
};
