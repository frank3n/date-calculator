# README #

Hi guys,
I tried to keep the code as readable as possible, if some things are not clear, then I hope that reading the tests descriptions will clarify any doubts.
Thanks

### How do I run it? ###

Just a standard `npm install` and `node ./index` should do it :)
FYI I'm on Node v8.0.0, if there are any problems running the app, I suggest using the same Node version as I did.