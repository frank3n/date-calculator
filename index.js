const prompt = require('prompt');
const hasValidDateFormat = require('./lib/hasValidDateFormat');
const calculateDaysDiff = require('./lib/calculateDaysDiff');

const startDateMessage = 'Please enter your start date in format YYYY-MM-DD';
const endDateMessage = 'Please enter your end date in format YYYY-MM-DD';

const errorMessage = 'You have entered an incorrect date, please try again (is your date between 1901-01-01 and 2999-12-31?)';

prompt.colors = false;
prompt.start();

prompt.get([{
  name: 'startDate',
  description: startDateMessage,
  required: true,
  format: 'date',
  conform: hasValidDateFormat,
  message: errorMessage
}, {
  name: 'endDate',
  description: endDateMessage,
  required: true,
  format: 'date',
  conform: hasValidDateFormat,
  message: errorMessage
}], (error, result) => {
  try {
    console.log(`There are ${calculateDaysDiff(result.startDate, result.endDate)} days between ${result.startDate} and ${result.endDate}`);
  } catch (exception) {
    console.error('Oooops, something went wrong...', exception);
  }
});
