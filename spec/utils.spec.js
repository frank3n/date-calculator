const expect = require('chai').expect;

const {isLeapYear, compareDates} = require('../lib/utils');

describe('isLeapYear', () => {
  describe('when year is dividable by 4', () => {
    it('should return true', () => {
      expect(isLeapYear(2004)).to.be.true;
    });

    describe('when year is dividable by 100', () => {
      describe('when year is dividable by 400', () => {
        it('should return true', () => {
          expect(isLeapYear(2000)).to.be.true;
        });
      });

      describe('when year is not dividable by 400', () => {
        it('should return false', () => {
          expect(isLeapYear(2100)).to.be.false;
        });
      });
    });
  });
});

describe('compareDates', () => {
  describe('when date A is before date B', () => {
    it('should return negative value', () => {
      expect(compareDates('2000-01-01', '2000-01-02')).to.equal(-1);
      expect(compareDates('2000-01-01', '2000-02-01')).to.equal(-1);
      expect(compareDates('2000-01-01', '2001-01-01')).to.equal(-1);
    });
  });

  describe('when date A is after date B', () => {
    it('should return positive value', () => {
      expect(compareDates('2000-01-02', '2000-01-01')).to.equal(1);
      expect(compareDates('2000-02-01', '2000-01-01')).to.equal(1);
      expect(compareDates('2001-01-01', '2000-01-01')).to.equal(1);
    });
  });

  describe('when date A is equal to date B', () => {
    it('should return 0', () => {
      expect(compareDates('2000-01-01', '2000-01-01')).to.equal(0);
    });
  });
});
