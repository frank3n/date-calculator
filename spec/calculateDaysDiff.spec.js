const expect = require('chai').expect;

const calculateDaysDiff = require('../lib/calculateDaysDiff');

describe('calculateDaysDiff', () => {
  describe('when dates are in the same year', () => {
    describe('when dates are is the same month', () => {
      it('should return the amount of days between given dates of the month', () => {
        expect(calculateDaysDiff('1901-01-01', '1901-01-11')).to.equal(9);
      });
    });

    describe('when dates are in different months', () => {
      it('should return the amount of days between months and days', () => {
        expect(calculateDaysDiff('1901-01-01', '1901-02-05')).to.equal(34);
      });

      describe('when February is in given range', () => {
        it('should make an adjustment for a shorter months', () => {
          expect(calculateDaysDiff('1901-01-01', '1901-03-01')).to.equal(58);
        });

        describe('when in leap year', () => {
          it('should make an adjustment for a longer February', () => {
            expect(calculateDaysDiff('2004-02-01', '2004-03-01')).to.equal(28);
          });
        });
      });
    });
  });

  describe('when dates are in different years', () => {
    describe('when dates are in different months', () => {
      it('should return the amount of days between given dates', () => {
        expect(calculateDaysDiff('1901-01-01', '1902-02-01')).to.equal(395);
        expect(calculateDaysDiff('2005-02-01', '2006-01-03')).to.equal(335);
        expect(calculateDaysDiff('2005-02-28', '2007-01-29')).to.equal(699);
        expect(calculateDaysDiff('2005-08-28', '2006-02-22')).to.equal(177);
        expect(calculateDaysDiff('2005-08-01', '2006-03-01')).to.equal(211);
      });
    });

    describe('when a full year has not passed', () => {
      it('should subtract remaining days from a full year', () => {
        expect(calculateDaysDiff('1901-12-24', '1902-04-15')).to.equal(111);
      });
    });

    describe('when leap years are in given range', () => {
      it('should make adjustments for leap years', () => {
        expect(calculateDaysDiff('1901-01-01', '1910-03-01')).to.equal(3345);
      });
    });
  });

  describe('when dates are far away from each other', () => {
    it('should take into account leap years', () => {
      expect(calculateDaysDiff('1901-12-29', '2501-07-30')).to.equal(218993);
    });
  });

  describe('when first date is after second date', () => {
    it('should return the amount of days between given dates', () => {
      expect(calculateDaysDiff('1901-01-05', '1901-01-01')).to.equal(3);
    });
  });

  describe('when testing against provided data', () => {
    it('should pass', () => {
      expect(calculateDaysDiff('1983-06-02', '1983-06-22')).to.equal(19);
      expect(calculateDaysDiff('1984-07-04', '1984-12-25')).to.equal(173);
      expect(calculateDaysDiff('1989-01-03', '1983-08-03')).to.equal(1979);
    });
  });

  describe('when first date is after second date', () => {
    it('should pass calculate date diff regardless or date argument order', () => {
      expect(calculateDaysDiff('2501-07-30', '1901-12-29')).to.equal(218993);
    });
  });
});
